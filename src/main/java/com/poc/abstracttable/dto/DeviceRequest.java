package com.poc.abstracttable.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceRequest {
    private int deviceId;
    private int agentId;
    private String firewallDescription;
}
