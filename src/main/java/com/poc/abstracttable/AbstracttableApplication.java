package com.poc.abstracttable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbstracttableApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbstracttableApplication.class, args);
	}

}
