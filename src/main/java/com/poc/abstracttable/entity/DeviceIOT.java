package com.poc.abstracttable.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeviceIOT extends Device {

    @OneToOne
    private Client client;

    private String firewallDescription;
}
