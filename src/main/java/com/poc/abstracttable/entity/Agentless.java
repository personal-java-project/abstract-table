package com.poc.abstracttable.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Agentless {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int agentlessId;

    private String description;

    private String address;

    private String hostname;
}
