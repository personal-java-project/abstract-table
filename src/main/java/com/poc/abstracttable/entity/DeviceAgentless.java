package com.poc.abstracttable.entity;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeviceAgentless extends Device {

    @OneToOne
    private Agentless agent;

    private String firewallDescription;
}
