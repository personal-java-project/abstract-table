package com.poc.abstracttable.service;

import com.poc.abstracttable.entity.*;
import com.poc.abstracttable.repo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.UUID;

@Service
@Slf4j
public class DeviceService {

    @Autowired
    private DeviceClientAgentRepository deviceClientAgentRepository;

    @Autowired
    private AgentRepository agentRepository;

    @Autowired
    private DeviceAgentlessRepository deviceAgentlessRepository;

    @Autowired
    private AgentlessRepository agentlessRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @PostConstruct
    private void init() {
        Client client = Client.builder()
                .address("10.10.10.10")
                .description("from haris pc")
                .externalClientId(UUID.randomUUID())
                .build();

        DeviceClientAgent deviceClientAgent = DeviceClientAgent.builder()
                .client(client)
                .firewallDescription("no firewall")
                .build();

        agentRepository.save(client);
//        deviceClientAgentRepository.save(deviceClientAgent);
        deviceRepository.save(deviceClientAgent);

        Agentless agentless = Agentless.builder()
                .address("1.1.1.1")
                .description("from hostname")
                .hostname("www.google.com")
                .build();

        DeviceAgentless deviceAgentless = DeviceAgentless.builder()
                .agent(agentless)
                .firewallDescription("no firewall")
                .build();

        agentlessRepository.save(agentless);
        deviceRepository.save(deviceAgentless);

        DeviceIOT deviceIOT = DeviceIOT.builder()
                .client(client)
                .firewallDescription("no firewall")
                .build();

        deviceRepository.save(deviceIOT);

    }

    public Device getDevice(int id) throws IOException {
        return deviceRepository.findById(id).orElseThrow(() -> new IOException("Device not found"));
    }

    public Device addDevice(Device device) {
        return deviceRepository.save(device);
    }

    public void deleteDevice(int id) {
        deviceRepository.deleteById(id);
    }
}
