package com.poc.abstracttable.controller;

import com.poc.abstracttable.dto.DeviceRequest;
import com.poc.abstracttable.entity.Device;
import com.poc.abstracttable.entity.DeviceAgentless;
import com.poc.abstracttable.service.DeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Slf4j
@RequestMapping("/api/v1/device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @GetMapping("/{id}")
    public Device getDevice(@PathVariable int id) throws IOException {
        return deviceService.getDevice(id);
    }

    @PostMapping
    public Device addDevice(@RequestBody Device deviceRequest) {
        return deviceService.addDevice(deviceRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteDevice(@PathVariable int id) {
        deviceService.deleteDevice(id);
    }
}
