package com.poc.abstracttable.repo;

import com.poc.abstracttable.entity.DeviceClientAgent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceClientAgentRepository extends JpaRepository<DeviceClientAgent, Integer> {
}
