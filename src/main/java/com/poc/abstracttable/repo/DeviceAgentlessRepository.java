package com.poc.abstracttable.repo;

import com.poc.abstracttable.entity.DeviceAgentless;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceAgentlessRepository extends JpaRepository<DeviceAgentless, Integer> {
}
