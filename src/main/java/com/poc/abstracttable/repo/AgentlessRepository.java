package com.poc.abstracttable.repo;

import com.poc.abstracttable.entity.Agentless;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentlessRepository extends JpaRepository<Agentless, Integer> {
}
