package com.poc.abstracttable.repo;

import com.poc.abstracttable.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentRepository extends JpaRepository<Client, Integer> {
}
